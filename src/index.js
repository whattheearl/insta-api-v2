const express = require('express')
const session = require('express-session')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')
var cors = require('cors');
require('dotenv').config()

// Environmental variables
const PORT = process.env.PORT || 5000
const MONGO_URI = process.env.MONGO_URI || 'mongodb://api-db/kg'

// Strategies
const GoogleStrategy = require('./passport/google')
passport.use(GoogleStrategy)

const MONGOOSE_CONFIG = { 
    useNewUrlParser: true, 
    useUnifiedTopology: true, 
    useCreateIndex: true 
}

// Passport and session setup
mongoose.connect(MONGO_URI, MONGOOSE_CONFIG)
        .then(console.log(`Connected to Mongoose @ ${MONGO_URI}`))
        .catch(err => console.log(err))

// App setup
const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Session setup
const sessionOption = require('./passport/session').option
app.use(session(sessionOption))
app.use(passport.initialize())
app.use(passport.session())

// Routes Setup
// API
app.use(cors());
app.use('/api/user', require('./routes/User'))
app.use('/api/post', require('./routes/Post'))
app.use('/api/comment', require('./routes/Comment'))
app.use('/api/like', require('./routes/Like'))
app.use('/api/self', require('./routes/Self'))
// Auth/Login/Logout
app.use('/auth', require('./routes/Auth'))

// Spin up server
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
})
.on('error', 
    err => console.log(err)
)

