var GoogleStrategy = require('passport-google-oauth20');
const User = require('../models/User')
const { uploadUrlToS3 } = require('../utilities/s3-util'); 
require('dotenv').config()

const GOOGLE_ID = process.env.GOOGLE_ID;
const GOOGLE_SECRET = process.env.GOOGLE_SECRET;
const GOOGLE_CALLBACK = process.env.GOOGLE_CALLBACK;

if (!GOOGLE_ID) {
    console.error("GOOGLE_ID not found");
}
if (!GOOGLE_SECRET) {
    console.error("GOOGLE_SECRET not found");
}
if (!GOOGLE_CALLBACK) {
    console.error("GOOGLE_CALLBACK not found");
}

module.exports = new GoogleStrategy(
    {
        clientID: GOOGLE_ID,
        clientSecret: GOOGLE_SECRET,
        callbackURL: GOOGLE_CALLBACK,
    },

    async (accessToken, refreshToken, profile, done) => {
        try {
            let existingUser = await User.findOne({'email': profile.emails[0].value});
            if(existingUser) {
                return done(null, existingUser)
            }

            // create new user
            let newUser = await User(
                {
                    google: {
                        id: profile.id,
                    },
                    name: profile.displayName,
                    email: profile.emails[0].value,
                })
                .save({new: true})
            
            // upload photo to s3
            let profilePhotoUrl = profile.photos[0].value
            let res = await uploadUrlToS3(String(newUser._id), profilePhotoUrl)
            
            // save photo location
            newUser.img = res.Location
            await newUser.save({new: true})

            // return user
            done(null, newUser)
        } catch(err) {
            // return error
            done(err)
        }
    }
)
