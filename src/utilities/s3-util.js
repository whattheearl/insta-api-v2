const urljoin = require('url-join')
const AWS = require('aws-sdk')
const axios = require('axios')
AWS.config.update({region: 'us-east-2'})

const s3 = new AWS.S3()

const AWS_BUCKET = process.env.AWS_BUCKET;
if (AWS_BUCKET === null || AWS_BUCKET === undefined) {
    console.log("ENV variable AWS_BUCKET is requires");
}

const uploadUrlToS3 = (userId, url) => {
    const key = urljoin('profile', `${userId}.jpg`)
    let requestOptions = {
        url: url,
        responseType:'stream'
    }
    return axios(requestOptions)
    .then(res => {
        const params = {Bucket: AWS_BUCKET, Key: key, Body: res.data}
        return s3.upload(params).promise()
    })
}

module.exports = { uploadUrlToS3 }