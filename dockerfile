FROM node:10-alpine

RUN ["apk", "add", "--update", "make", "python"]

WORKDIR /app

COPY package*.json ./

RUN ["npm", "i", "-g", "nodemon"]

RUN ["npm", "ci"]

COPY ./src ./src

CMD ["nodemon", "/app/src/index.js"]